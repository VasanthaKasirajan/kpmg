﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KPMG.UI.Startup))]
namespace KPMG.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
