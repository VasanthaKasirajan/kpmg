﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPMG.UI.Models
{
    public class QueryResultModel
    {
        public int TotalCount { get; set; }
        public List<Accounts> Result { get; set; }
    }
}