﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KPMG.UI.Models
{

    public class Accounts
    {
      public int Id { get; set; }


        public string Account { get; set; }

        public string Description { get; set; }

        public string CurrencyCode { get; set; }


        public decimal Amount { get; set; }

    }
}