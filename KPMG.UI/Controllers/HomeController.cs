﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using PagedList;
using System.Linq;
using KPMG.UI.Models;

namespace KPMG.UI.Controllers
{
    public class HomeController : Controller
    {      

        public ActionResult Index(int? page)
        {


            int pageSize = 50;
            int pageNumber = (page ?? 0);

            using (var client = new HttpClient())
            {
                var url = $"http://localhost:16834/api/Transaction?pageSize={pageSize}&pageNumber={pageNumber}";
                var response = (client.GetAsync(url).Result);
                var content = response.Content.ReadAsStringAsync().Result;
                
                var model = JsonConvert.DeserializeObject<QueryResultModel>(content);

                var resultAsIPagedList = new StaticPagedList<Accounts>(model.Result, pageNumber+1, pageSize, model.TotalCount);

                return View(resultAsIPagedList);
            }
               

           
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Upload()
        {
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {

                if (upload != null && upload.ContentLength > 0)
                {

                    if (upload.FileName.EndsWith(".csv"))
                    {

                        var target = new MemoryStream();
                        upload.InputStream.CopyTo(target);
                        byte[] data = target.ToArray();
                        HttpContent fileContent = new ByteArrayContent(data);

                        dynamic content = null;
                        var model = new resultMessage();
                        using (var client = new HttpClient())
                        {
                            client.Timeout = TimeSpan.FromMinutes(20);
                            using (var formData = new MultipartFormDataContent())
                            {
                                formData.Add(fileContent, "file", "fileName");

                                //call service
                                var response = client.PostAsync("http://localhost:16834/api/Transaction/uploadFile", formData).Result;

                                if (!response.IsSuccessStatusCode)
                                {
                                    throw new Exception();
                                }
                                else
                                {  
                                    var stream = response.Content.ReadAsStringAsync();
                                    content = stream.Result;
                                    model = JsonConvert.DeserializeObject<resultMessage>(content);
                                }
                            }
                        }
                       
                        return View(model);
                        
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            return View();
        }
    }

    public class resultMessage
    {
        public int successCount;

        public List<string> errors;

        public List<KPMG.UI.Models.Accounts> resultAccounts;
    }

}