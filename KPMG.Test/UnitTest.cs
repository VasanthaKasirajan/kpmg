﻿using System;
using System.Web;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KPMG.UI.Controllers;
using Moq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;

namespace KPMG.Test
{
    [TestClass]
    public class UnitTest
    {

        
        [TestMethod]
        public void TestMethod_verfify_ModelError()
        {
            //Arrange
            HomeController controller = new HomeController();          
            var mock =  new Mock<HttpPostedFileBase>().Object;
           
            //Act
            var actual = controller.Upload(mock);
            
            //Assert
            Assert.IsInstanceOfType(actual, typeof(ViewResult));
        }

        [TestMethod]
        public void TestMethod_verfify_FileContents()
        {
            //Arrange
            HomeController controller = new HomeController();         

            var filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"TESTCSV.csv");            
            var _stream = new FileStream(string.Format(
                        filePath,
                       AppDomain.CurrentDomain.BaseDirectory),
                    FileMode.Open);

            var context = new Mock<HttpContextBase>();
            var request = new Mock<HttpRequestBase>();
            var files = new Mock<HttpFileCollectionBase>();
            var file = new Mock<HttpPostedFileBase>();
            context.Setup(x => x.Request).Returns(request.Object);

            files.Setup(x => x.Count).Returns(1);           
            file.Setup(x => x.InputStream).Returns(_stream);
            file.Setup(x => x.ContentLength).Returns((int)_stream.Length);
            file.Setup(x => x.FileName).Returns(_stream.Name);

            files.Setup(x => x.Get(0).InputStream).Returns(file.Object.InputStream);
            request.Setup(x => x.Files).Returns(files.Object);
            request.Setup(x => x.Files[0]).Returns(file.Object);

            controller.ControllerContext = new ControllerContext(
                                     context.Object, new RouteData(), controller);



            //Act
            var actual =(ViewResult)controller.Upload(file.Object);
            var model = (KPMG.UI.Controllers.resultMessage)actual.ViewData.Model;

            //Assert
            Assert.AreEqual(model.successCount, 0);
            Assert.AreEqual(model.errors[0].ToString(), "Row 0 currency code value is invalid");
            Assert.AreEqual(model.errors[1].ToString(), "Row 1 currency code value is invalid");

            
        }
    }


   

}
