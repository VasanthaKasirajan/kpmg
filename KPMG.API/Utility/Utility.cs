﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KPMG.API.Utility
{
    public static class Utility
    {
        public static bool ValidateRow(int i, DataRow row, List<string> errorList, List<string> currencyCodes)
        {
            var isValid = true;
            if (string.IsNullOrEmpty(row["Account"].ToString()))
            {
                isValid = false;
                errorList.Add($"Row {i} account value is empty");
            }
            if (string.IsNullOrEmpty(row["Description"].ToString()))
            {
                isValid = false;
                errorList.Add($"Row {i} description value is empty");
            }
            if (string.IsNullOrEmpty(row["Currency Code"].ToString()))
            {
                isValid = false;
                errorList.Add($"Row {i} currency code value is empty");
            }
            //validate currency code
            if (!string.IsNullOrEmpty(row["Currency Code"].ToString()) && !currencyCodes.Any(x => x == row["Currency Code"].ToString()))
            {
                isValid = false;
                errorList.Add($"Row {i} currency code value is invalid");
            }
            if (string.IsNullOrEmpty(row["Amount"].ToString()))
            {
                isValid = false;
                errorList.Add($"Row {i} amount value is empty");
            }

            if (!string.IsNullOrEmpty(row["Amount"].ToString()))
            {

                string value = row["Amount"].ToString();
                decimal number;
                if (!Decimal.TryParse(value, out number))
                {
                    isValid = false;
                    errorList.Add($"Row {i} of the imported  amount value is invalid" + value);
                }


                //TODO: parse amount value for decimal;
                //IsValid = false;
                //errorList.Add($"Row {i} amount value is invalid");
            }

            return isValid;
        }
    }
}