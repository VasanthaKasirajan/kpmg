﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.API.Models;

namespace KPMG.API.Repository
{
    public interface IAccountRepository
    {
        void Save(List<Accounts> account);
        List<string> GetCurrencyCodes();

        IQueryable<Accounts> GetAll();
    }
}