﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using KPMG.API.Models;

namespace KPMG.API.Repository
{
    public class Context : DbContext
    {
        public Context() : base("name= TransactionDal")
        {
            Configuration.AutoDetectChangesEnabled = false;
        }

        public DbSet<Accounts> Account { get; set; }
        public DbSet<IsoCurrencyCodes> CurrencyCode { get; set; }

    }
}