﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.API.Models;

namespace KPMG.API.Repository
{
    public class AccountRepository : IAccountRepository
    {
        Context context = new Context();
        

        public void Save(List<Accounts> accounts)
        {
            //context.Configuration.AutoDetectChangesEnabled = false;
            //if (account.Id == 0)
            //{
            //    context.Account.Add(account);
            //    context.SaveChanges();

            //}
            
            
            if (accounts.Any())
            {
                context.Account.AddRange(accounts);
                
                context.SaveChanges();
            }


        }
        public List<string> GetCurrencyCodes()
        {
            return context.CurrencyCode.Select(x => x.CurrencyCode).ToList();
        }

        public IQueryable<Accounts> GetAll()
        {
            return context.Account;
        }
    }
}