﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KPMG.API.Models
{
    public class Accounts
    {

        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        public string Account { get; set; }

        public string Description { get; set; }

        [Required]
        public string CurrencyCode { get; set; }

        [Required]
        public decimal Amount { get; set; }

    }
}