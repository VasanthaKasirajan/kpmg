﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPMG.API.Models
{
    public class IsoCurrencyCodes
    {
        public int Id { get; set; }
        public string CurrencyCode { get; set; }
    }
}