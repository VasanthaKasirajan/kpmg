﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Data;
using System.Threading.Tasks;
using KPMG.API.Models;
using KPMG.API.Repository;


namespace KPMG.API.Controllers
{
    public class TransactionController : ApiController
    {

        private List<string> currencyCodes = new List<string>();
        private IAccountRepository accountRepository;

        public TransactionController(IAccountRepository _accountRepository)
        {
            this.accountRepository = _accountRepository;
            currencyCodes = _accountRepository.GetCurrencyCodes();
        }

        public QueryResultModel GET(int pageSize, int pageNumber)
        {
            return new QueryResultModel
            {
                Result = accountRepository.GetAll().OrderByDescending(x => x.Id).Skip(pageSize * pageNumber).Take(pageSize).ToList(),
                TotalCount = accountRepository.GetAll().Count()
            };

        }

        //public HttpResponseMessage GET()
        //{
        //    return new HttpResponseMessage(HttpStatusCode.OK);
        //}

        public async Task<IHttpActionResult> uploadFile()
        {           
            
            if (!Request.Content.IsMimeMultipartContent())
                throw new Exception();

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            var file = provider.Contents.First();
            var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
            var buffer = await file.ReadAsByteArrayAsync();
            var stream = new MemoryStream(buffer);

            DataTable csvTable = new DataTable();
            using (CsvReader csvReader =
                new CsvReader(new StreamReader(stream), true))
            {
                csvTable.Load(csvReader);
                var accountList = new List<Accounts>();
                var accountInvalidList = new List<Accounts>();
                var errorList = new List<string>();

                for (int i = 0; i<csvTable.Rows.Count; i++)
                {
                    var row = csvTable.Rows[i];

                    if (Utility.Utility.ValidateRow(i, row, errorList, currencyCodes))
                    {
                        var accounts = new Accounts
                        {
                            Account = row["Account"].ToString(),
                            Description = row["Description"].ToString(),
                            CurrencyCode = row["Currency Code"].ToString(),
                            Amount = Convert.ToDecimal(row["Amount"])

                        };
                        accountList.Add(accounts);
                    }
                    
                }
                accountRepository.Save(accountList);

               
                var resultMessage = new resultMessage
                {
                    successCount = accountList.Count,
                    errors = errorList,
                    resultAccounts = accountList
                };
                return Ok(resultMessage);

            }
        }

    }

    public class resultMessage
    {
        public int successCount;

        public List<string> errors;

        public List<Accounts> resultAccounts;
    }
}
