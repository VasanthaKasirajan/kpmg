# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

ASP.NET MVC Software Developer Assignment for KPMG

### How do I get set up? ###

* KPMG.UI proj - Interface to upload csv file
* KPMG.API proj- Api to push and pull the details from sql through EF
* KPMG.Test proj - Unit Test
* DBScripts Folder - DB and Table creation scripts
* SampleData - Sample csv file used for upload


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact