USE [Transaction]
GO

/****** Object:  Table [dbo].[IsoCurrencyCodes]    Script Date: 28/01/2017 08:51:57 ******/
DROP TABLE [dbo].[IsoCurrencyCodes]
GO

/****** Object:  Table [dbo].[IsoCurrencyCodes]    Script Date: 28/01/2017 08:51:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IsoCurrencyCodes](
	[Id] [int] NOT NULL,
	[CurrencyCode] [nvarchar](3) NULL
) ON [PRIMARY]

GO


