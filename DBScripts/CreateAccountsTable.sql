USE [Transaction]
GO

/****** Object:  Table [dbo].[Accounts]    Script Date: 28/01/2017 08:51:33 ******/
DROP TABLE [dbo].[Accounts]
GO

/****** Object:  Table [dbo].[Accounts]    Script Date: 28/01/2017 08:51:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Accounts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Account] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
	[CurrencyCode] [nvarchar](5) NOT NULL,
	[Amount] [decimal](18, 3) NOT NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


